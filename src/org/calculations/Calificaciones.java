package org.calculations;

public class Calificaciones {
    public static void main(String[] args) {
        Alumno al = new Alumno();
        al.setNombre("Pedro Perez");
        al.setCalificaciones(al.generateRandomCal());
        String nombre = al.getNombre();
        double[] calificaciones = al.getCalificaciones();
        double promedio = al.calcularPromedio(calificaciones);
        char calificacionFinal = al.calcularCalificacionFinal(promedio);
        al.imprimirCalificacion(nombre, promedio, calificaciones, calificacionFinal);
    }
}
