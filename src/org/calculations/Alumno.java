package org.calculations;

import java.text.MessageFormat;

public class Alumno {

    private String nombre;

    private double[] calificaciones = new double[5];

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double[] getCalificaciones() {
        return calificaciones;
    }

    public void setCalificaciones(double[] calificaciones) {
        this.calificaciones = calificaciones;
    }

    public double calcularPromedio(double[] calificaciones){
        double suma = 0;
        for(double calificacion: calificaciones){
            suma+= calificacion;
        }
        suma = suma / calificaciones.length;
        return suma;
    }

    public char calcularCalificacionFinal(double promedio){

        char calificacionFinal;

        if (promedio >= 91 && promedio <= 100 ){
            calificacionFinal = 'A';
        }

        else if (promedio >= 81 && promedio <= 90 ){
            calificacionFinal = 'B';
        }

        else if (promedio >= 71 && promedio <= 80 ){
            calificacionFinal = 'C';
        }

        else if (promedio >= 61 && promedio <= 70 ){
            calificacionFinal = 'E';
        }

        else {
            calificacionFinal = 'F';
        }

        return calificacionFinal;
    }

    public void imprimirCalificacion(String nombre, double promedio, double[] calificaciones, char calificacion) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append(MessageFormat.format("Nombre del estudiante: {0}\n", nombre));
        for(int i = 0; i < calificaciones.length; i++){
            mensaje.append(MessageFormat.format("Cálificacion {0}: {1}\n", i + 1, calificaciones[i]));
        }
        mensaje.append(MessageFormat.format("Promedio: {0}\n", promedio));
        mensaje.append(MessageFormat.format("Cálificacion: {0}\n", calificacion));
        System.out.println(mensaje);
    }

    public  double[] generateRandomCal(){
        double[] randomCal = new double[5];
        for (int i = 0; i < randomCal.length; i++){
            randomCal[i] = Math.random() * 100;
        }
        return randomCal;
    }
}
